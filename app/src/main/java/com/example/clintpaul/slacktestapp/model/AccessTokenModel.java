package com.example.clintpaul.slacktestapp.model;

import com.google.gson.annotations.SerializedName;

public class AccessTokenModel {


    @SerializedName("ok")
    private String ok;

    @SerializedName("access_token")
    private String access_token;
    @SerializedName("scope")
    private String scope;
    @SerializedName("user_id")
    private String user_id;
    /*@SerializedName("user")
    private UserModel userModel;*/



   /* public UserModel getUserModel() {
        return userModel;
    }

    public void setUserModel(UserModel userModel) {
        this.userModel = userModel;
    }*/

    public String getAccess_token() {
        return access_token;
    }

    public void setAccess_token(String access_token) {
        this.access_token = access_token;
    }

    public String getScope() {
        return scope;
    }

    public void setScope(String scope) {
        this.scope = scope;
    }

    public String getOk() {
        return ok;
    }

    public void setOk(String ok) {
        this.ok = ok;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    /*public class UserModel {

        @SerializedName("name")
        private String name;
        @SerializedName("id")
        private int id;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;

        }
    }*/
}
