package com.example.clintpaul.slacktestapp.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.example.clintpaul.slacktestapp.R;
import com.example.clintpaul.slacktestapp.base.BaseRecyclerViewAdapter;
import com.example.clintpaul.slacktestapp.model.ChannelInfoModel;
import com.example.clintpaul.slacktestapp.viewholder.ChannelListViewHolder;

import java.util.List;

public class ChannelListAdapter extends BaseRecyclerViewAdapter<ChannelInfoModel.ChannelDetails> {


    public ChannelListAdapter(RecyclerView rv_channels, List<ChannelInfoModel.ChannelDetails> channelDetails, Context context) {


        super(rv_channels,channelDetails,context);

    }

    @Override
    protected RecyclerView.ViewHolder createItemViewHolder(ViewGroup parent) {
        return new ChannelListViewHolder(
                LayoutInflater.from(parent.getContext()).inflate(R.layout.rv_channels_list, parent, false));
    }

    @Override protected void bindItemViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        ((ChannelListViewHolder) viewHolder).bind(items.get(position), context);
    }
}
