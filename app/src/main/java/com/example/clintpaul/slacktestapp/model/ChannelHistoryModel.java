package com.example.clintpaul.slacktestapp.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ChannelHistoryModel {

    @SerializedName("ok")
    private boolean ok;
    @SerializedName("latest")
    private String latest;
    @SerializedName("messages")
    private List<MessageFromChannel> messageFromChannel;

    public static class MessageFromChannel {

        @SerializedName("type")
        private String  type;
        @SerializedName("user")
        private String user;
        @SerializedName("text")
        private String text;
        @SerializedName("ts")
        private String ts;
        @SerializedName("username")
        private String username;

        public MessageFromChannel(String user, String text, String type, String ts, String username) {

            this.user = user;
            this.text = text;
            this.type = type;
            this.ts = ts;
            this.username = username;
        }

        public MessageFromChannel(String user, String text) {

            this.user = user;
            this.text = text;
        }


        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getUser() {
            return user;
        }

        public void setUser(String user) {
            this.user = user;
        }

        public String getText() {
            return text;
        }

        public void setText(String text) {
            this.text = text;
        }

        public String getTs() {
            return ts;
        }

        public void setTs(String ts) {
            this.ts = ts;
        }
    }

    @SerializedName("has_more")
    private boolean has_more;

    public boolean isOk() {
        return ok;
    }

    public void setOk(boolean ok) {
        this.ok = ok;
    }

    public String getLatest() {
        return latest;
    }

    public void setLatest(String latest) {
        this.latest = latest;
    }

    public List<MessageFromChannel> getMessageFromChannel() {
        return messageFromChannel;
    }

    public void setMessageFromChannel(List<MessageFromChannel> messageFromChannel) {
        this.messageFromChannel = messageFromChannel;
    }

    public boolean isHas_more() {
        return has_more;
    }

    public void setHas_more(boolean has_more) {
        this.has_more = has_more;
    }
}
