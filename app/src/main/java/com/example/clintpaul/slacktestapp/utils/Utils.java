package com.example.clintpaul.slacktestapp.utils;

import android.support.design.widget.TextInputLayout;
import android.text.Editable;
import android.widget.EditText;
import android.widget.TextView;

public class Utils {


    public static String getString(Object param) {
        try {
            if (param instanceof TextInputLayout) {
                TextInputLayout textInputLayout = (TextInputLayout) param;
                if (textInputLayout.getEditText() != null) {
                    return (eliminateString(textInputLayout.getEditText().getText().toString().trim()));
                }
            }
            if (param instanceof EditText)
                return eliminateString(((EditText) param).getText().toString().trim());
            if (param instanceof TextView)
                return eliminateString(((TextView) param).getText().toString().trim());
            if (param instanceof String) return eliminateString(((String) param).trim());
            if (param instanceof Integer) return eliminateString((String.valueOf(param).trim()));
            if (param instanceof Editable) return param.toString().trim();
            if (param instanceof CharSequence) return param.toString().trim();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    private static String eliminateString(String s) {
        if (s.equalsIgnoreCase("null")) {
            return "";
        } else return s.trim();
    }

    public static Boolean isNotEmpty(Object param) {
        try {
            String field = getString(param);
            return !(field.trim().isEmpty() || field.trim().equals("") || field.equals("null"));
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return false;
        }
    }
}
