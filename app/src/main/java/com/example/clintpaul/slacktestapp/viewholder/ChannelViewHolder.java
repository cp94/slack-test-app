package com.example.clintpaul.slacktestapp.viewholder;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.clintpaul.slacktestapp.R;
import com.example.clintpaul.slacktestapp.model.ChannelHistoryModel;
import com.example.clintpaul.slacktestapp.model.UserModel;
import com.example.clintpaul.slacktestapp.network.retrofit.GetDataService;
import com.example.clintpaul.slacktestapp.network.retrofit.RetrofitClientInstance;
import com.example.clintpaul.slacktestapp.utils.SharedPref;
import com.example.clintpaul.slacktestapp.utils.Utils;
import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChannelViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.txt_user_name)
    TextView txt_user_name;
    @BindView(R.id.txt_message)
    TextView txt_message;
    @BindView(R.id.progressBar_1)
    ProgressBar progressBar_1;
    private ChannelHistoryModel.MessageFromChannel messageFromChannel;
    private UserModel userModel;

    public ChannelViewHolder(@NonNull View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

    public void bind(ChannelHistoryModel.MessageFromChannel messageFromChannel) {

        this.messageFromChannel = messageFromChannel;
        txt_message.setText(messageFromChannel.getText());

        getUserDetails();

    }

    private void getUserDetails() {

        GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
        Call<UserModel> userModelCall = service.userDetails(SharedPref.getAccessToken(),
                messageFromChannel.getUser()
                );
        userModelCall.enqueue(new Callback<UserModel>() {
            @Override
            public void onResponse(@NonNull Call<UserModel> call, @NonNull Response<UserModel> response) {

                if (response.isSuccessful()) {

                    userModel = response.body();
                    assert userModel != null;
                    progressBar_1.setVisibility(View.GONE);
                    txt_user_name.setVisibility(View.VISIBLE);

                    if (Utils.isNotEmpty(userModel.getUser().getName())) {

                        txt_user_name.setText(userModel.getUser().getName());
                    }


                }
            }

            @Override
            public void onFailure(@NonNull Call<UserModel> call, @NonNull Throwable t) {

            }
        });
    }
}
