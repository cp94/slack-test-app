package com.example.clintpaul.slacktestapp.base;

import android.content.Context;
import android.os.Handler;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.clintpaul.slacktestapp.R;
import com.example.clintpaul.slacktestapp.viewholder.ProgressViewHolder;

import java.util.ArrayList;
import java.util.List;

public abstract class BaseRecyclerViewAdapter<T> extends RecyclerView.Adapter<RecyclerView.ViewHolder>
{

    protected List<T> items;
    private static final int VIEW_ITEM = 1;
    private static final int VIEW_PROGRESS = 0;
    private static final int VISIBLE_THRESHOLD = 1;
    private int lastVisibleItem, totalItemCount;
    private RecyclerView recyclerView;
    protected Context context;
    private boolean loading;
//    private OnLoadMoreListener mOnLoadMoreListener;

    protected void setList(List<T> items) {
        this.items = items;
    }

    protected BaseRecyclerViewAdapter() {

    }

    protected BaseRecyclerViewAdapter(RecyclerView recyclerView, List<T> items, Context context) {
        this.recyclerView = recyclerView;
        setOnScrollListener(recyclerView);
        this.context = context;
        this.items = new ArrayList<>();
        this.items.clear();
        this.items.addAll(items);
    }

    protected BaseRecyclerViewAdapter(List<T> items, Context context) {
        this.items = items;
        this.context = context;
    }

    protected BaseRecyclerViewAdapter(List<T> items) {
        this.items = items;
    }

    protected void setOnScrollListener(RecyclerView recyclerView) {
        if (recyclerView != null && recyclerView.getLayoutManager() instanceof LinearLayoutManager) {
            this.recyclerView = recyclerView;
            final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
            recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);
                    totalItemCount = linearLayoutManager.getItemCount();
                    lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
                    /*if (!loading && totalItemCount <= (lastVisibleItem + VISIBLE_THRESHOLD)) {
                        if (mOnLoadMoreListener != null) {
                            mOnLoadMoreListener.onLoadMore();
                        }
                        loading = true;
                    }*/
                }
            });
        }
    }

    @Override public int getItemViewType(int position) {
        return items.get(position) != null ? VIEW_ITEM : VIEW_PROGRESS;
    }

    /*public void setOnLoadMoreListener(OnLoadMoreListener loadMoreListener) {
        this.mOnLoadMoreListener = loadMoreListener;
    }*/

    protected RecyclerView.ViewHolder getProgress(ViewGroup parent) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.progress_bar_recycler, parent, false);

        return new ProgressViewHolder(view);
    }

    protected void showProgress(RecyclerView.ViewHolder holder) {
        ((ProgressViewHolder) holder).progressBar.setIndeterminate(true);
    }

    public void setLoaded() {
        loading = false;
    }

    public void setLoading() {
        this.loading = true;
    }

    public void stopLoading() {
        if (items.size() - 1 != -1) items.remove(items.size() - 1);
        notifyItemRemoved(items.size());
        notifyDataSetChanged();
        loading = false;
        recyclerView.invalidate();
    }

    private void removeLoader() {
        int position = items.size() - 1;
        T item = getItem(position);

        if (item == null) {
            items.remove(position);
            notifyItemRemoved(position);
        }
    }

    public T getItem(int position) {
        return items.get(position);
    }

/*    private void removeLoader() {
        if (items.size() - 1 != -1) items.remove(items.size() - 1);
        notifyItemRemoved(items.size());

        for (int i = 0; i < items.size(); i++) {
            if (items.get(i) == null) {
                items.remove(i);
            }
        }
    }*/

    public void addLoading() {
        items.add(null);
        Handler handler = new Handler();
        final Runnable r = new Runnable() {
            @Override public void run() {
                notifyItemInserted(items.size() - 1);
            }
        };
        handler.post(r);
    }

    public void addDataSet(List<T> dataSet) {
        Log.e("addDataSet","called");
        removeLoader();
        addAll(dataSet);
        notifyDataSetChanged();
        setLoaded();
        recyclerView.invalidate();
    }

    public void add(T item) {
        items.add(item);
        notifyItemInserted(items.size() - 1);
    }

    public void addAll(List<T> items) {
        for (T item : items) {
            add(item);
        }
        setLoaded();
    }

    public void remove(T item) {
        int position = items.indexOf(item);
        if (position > -1) {
            items.remove(position);
            notifyItemRemoved(position);
        }
    }

    public boolean isLoading() {
        return !loading;
    }

    public void remove(int pos) {
        notifyItemRemoved(pos);
    }

    public void clearItems() {
        items.clear();
        notifyDataSetChanged();
    }

    @Override public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder = null;
        switch (viewType) {
            case VIEW_ITEM:
                viewHolder = createItemViewHolder(parent);
                break;
            case VIEW_PROGRESS:
                viewHolder = createProgressViewHolder(parent);
                break;
            default:
                break;
        }
        return viewHolder;
    }

    @Override public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        switch (getItemViewType(position)) {
            case VIEW_ITEM:
                bindItemViewHolder(viewHolder, position);
                break;
            case VIEW_PROGRESS:
                bindProgressViewHolder(viewHolder);
            default:
                break;
        }
    }

    protected abstract RecyclerView.ViewHolder createItemViewHolder(ViewGroup parent);

    private RecyclerView.ViewHolder createProgressViewHolder(ViewGroup parent) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.progress_bar_recycler, parent, false);
        return new ProgressViewHolder(view);
    }

    public void changeDataSet(List<T> items) {
        this.items = items;
        notifyDataSetChanged();
    }

    protected abstract void bindItemViewHolder(RecyclerView.ViewHolder viewHolder, int position);

    private void bindProgressViewHolder(RecyclerView.ViewHolder viewHolder) {
        if (viewHolder != null) ((ProgressViewHolder) viewHolder).progressBar.setIndeterminate(true);
    }

    @Override public int getItemCount() {
        return items.size();
    }
}
