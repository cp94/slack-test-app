package com.example.clintpaul.slacktestapp.viewholder;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.example.clintpaul.slacktestapp.R;
import com.example.clintpaul.slacktestapp.activity.ChannelActivity;
import com.example.clintpaul.slacktestapp.model.ChannelInfoModel;
import com.example.clintpaul.slacktestapp.model.RtmBaseModel;
import com.example.clintpaul.slacktestapp.network.retrofit.GetDataService;
import com.example.clintpaul.slacktestapp.network.retrofit.RetrofitClientInstance;
import com.example.clintpaul.slacktestapp.utils.SharedPref;
import com.example.clintpaul.slacktestapp.utils.Utils;
import com.google.gson.Gson;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChannelListViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.channel_name)
    TextView txt_channel_name;
    @BindView(R.id.channel_desc)
    TextView txt_channel_desc;

    private RtmBaseModel rtmBaseModel;

    public ChannelListViewHolder(@NonNull View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

    public void bind(final ChannelInfoModel.ChannelDetails channelDetails, final Context context) {

        txt_channel_name.setText(channelDetails.getName());
        txt_channel_desc.setVisibility(Utils.isNotEmpty(channelDetails.getPurpose().getValue()) ? View.VISIBLE : View.GONE);
        txt_channel_desc.setText(Utils.isNotEmpty(channelDetails.getPurpose().getValue()) ? channelDetails.getPurpose().getValue() : "");

        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(context, ChannelActivity.class);
                intent.putExtra("channel", channelDetails.getId());
                intent.putExtra("channel_name", channelDetails.getName());
                context.startActivity(intent);
//                checkRtmStatus();
            }
        });

    }

    private void checkRtmStatus() {

        GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
        Call<RtmBaseModel> checkRtmSuccess = service.checkRtmSuccess(SharedPref.getAccessToken());
        checkRtmSuccess.enqueue(new Callback<RtmBaseModel>() {
            @Override
            public void onResponse(Call<RtmBaseModel> call, Response<RtmBaseModel> response) {

                if (response.isSuccessful()) {

                    rtmBaseModel = response.body();
                    Log.e("rtmBaseModel", new Gson().toJson(rtmBaseModel));

                }
            }

            @Override
            public void onFailure(Call<RtmBaseModel> call, Throwable t) {

            }
        });
    }
}
