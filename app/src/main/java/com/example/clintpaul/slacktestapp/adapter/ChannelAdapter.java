package com.example.clintpaul.slacktestapp.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.example.clintpaul.slacktestapp.R;
import com.example.clintpaul.slacktestapp.base.BaseRecyclerViewAdapter;
import com.example.clintpaul.slacktestapp.model.ChannelHistoryModel;
import com.example.clintpaul.slacktestapp.viewholder.ChannelListViewHolder;
import com.example.clintpaul.slacktestapp.viewholder.ChannelViewHolder;

import java.util.List;

import okhttp3.WebSocket;

public class ChannelAdapter extends BaseRecyclerViewAdapter<ChannelHistoryModel.MessageFromChannel> {




    public ChannelAdapter(RecyclerView rv_channels_main, List<ChannelHistoryModel.MessageFromChannel> messageFromChannel, Context context) {
        super(rv_channels_main,messageFromChannel, context);
    }

    @Override
    protected RecyclerView.ViewHolder createItemViewHolder(ViewGroup parent) {
        return new ChannelViewHolder(
                LayoutInflater.from(parent.getContext()).inflate(R.layout.rv_channel, parent, false));
    }

    @Override protected void bindItemViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        ((ChannelViewHolder) viewHolder).bind(items.get(position));
    }
}
