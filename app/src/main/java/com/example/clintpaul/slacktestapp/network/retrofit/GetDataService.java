package com.example.clintpaul.slacktestapp.network.retrofit;

import com.example.clintpaul.slacktestapp.model.AccessTokenModel;
import com.example.clintpaul.slacktestapp.model.ChannelHistoryModel;
import com.example.clintpaul.slacktestapp.model.ChannelInfoModel;
import com.example.clintpaul.slacktestapp.model.RtmBaseModel;
import com.example.clintpaul.slacktestapp.model.UserModel;
import com.example.clintpaul.slacktestapp.utils.Appconst;
import com.google.gson.JsonObject;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface GetDataService {


    @GET("oauth/authorize")
    Call<JsonObject> authorize(
            @Query(Appconst.CLIENT_ID_KEY) String client_id,
            @Query(Appconst.SCOPE_KEY) String scope,
            @Query(Appconst.REDIRECT_URI_KEY) String redirect_uri,
            @Query(Appconst.STATE_KEY) String state,
            @Query(Appconst.TEAM_KEY) String team

    );


    @FormUrlEncoded
    @POST("oauth.access")
    Call<AccessTokenModel> getAccessToken(
            @Field(Appconst.CLIENT_ID_KEY) String client_id,
            @Field(Appconst.CLIENT_SECRET_KEY) String client_secret,
            @Field(Appconst.CODE_KEY) String code,
            @Field(Appconst.REDIRECT_URI_KEY) String redirect_uri,
            @Field(Appconst.SINGLE_CHANNEL_KEY) boolean single_channel

    );

    @GET("channels.list")
    Call<ChannelInfoModel> getChannelInfo(
            @Query("token") String token
    );

    @GET("rtm.connect")
    Call<RtmBaseModel> checkRtmSuccess(
            @Query("token") String token
    );

    @GET("channels.history")
    Call<ChannelHistoryModel> channelHistory(
            @Query("token") String token,
            @Query("channel") String channel
    );

    @GET("users.info")
    Call<UserModel> userDetails(
            @Query("token") String token,
            @Query("user") String user
    );










}
