package com.example.clintpaul.slacktestapp.base;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.Toast;

import com.example.clintpaul.slacktestapp.R;
import com.example.clintpaul.slacktestapp.utils.Utils;

public class BaseActivity extends AppCompatActivity {


    public Toolbar toolbar;


    public void setToolBar(String title, String subtitle)
    {

        try {
            toolbar = findViewById(R.id.toolbar);
            if (toolbar != null) {
                toolbar.setTitle(Utils.getString(title));
                toolbar.setSubtitle(Utils.getString(subtitle));
                setSupportActionBar(toolbar);
                if (getSupportActionBar() != null) {
                    getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }



    public void setToolBarWithoutBack(String title) {

        try {
            toolbar = findViewById(R.id.toolbar);
            if (toolbar != null) {
                toolbar.setTitle(Utils.getString(title));
                setSupportActionBar(toolbar);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base);

    }


    public void showToast(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }
}
