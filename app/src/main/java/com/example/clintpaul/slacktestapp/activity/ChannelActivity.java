package com.example.clintpaul.slacktestapp.activity;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.clintpaul.slacktestapp.R;
import com.example.clintpaul.slacktestapp.adapter.ChannelAdapter;
import com.example.clintpaul.slacktestapp.base.BaseActivity;
import com.example.clintpaul.slacktestapp.model.ChannelHistoryModel;
import com.example.clintpaul.slacktestapp.model.MessageModel;
import com.example.clintpaul.slacktestapp.model.MessageRecieveModel;
import com.example.clintpaul.slacktestapp.model.RtmBaseModel;
import com.example.clintpaul.slacktestapp.model.UserModel;
import com.example.clintpaul.slacktestapp.network.retrofit.GetDataService;
import com.example.clintpaul.slacktestapp.network.retrofit.RetrofitClientInstance;
import com.example.clintpaul.slacktestapp.utils.Appconst;
import com.example.clintpaul.slacktestapp.utils.EchoWebSocketListener;
import com.example.clintpaul.slacktestapp.utils.SharedPref;
import com.example.clintpaul.slacktestapp.utils.Utils;
import com.google.gson.Gson;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.WebSocket;
import okhttp3.WebSocketListener;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChannelActivity extends BaseActivity implements View.OnClickListener {

    private RtmBaseModel rtmBaseModel;
    private String channel;
    private String channel_name;
    private EchoWebSocketListener echoWebSocketListener;
    private ChannelHistoryModel channelHistoryModel;
    private ChannelAdapter channelAdapter;
    private Context context;
    private WebSocket webSocket;
    private NotificationReceiver nReceiver;
    private String notification_text;
    private int count = 0;


    @BindView(R.id.rv_channels_main)
    RecyclerView rv_channels_main;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.txt_channel_name)
    TextView txt_channel_name;
    @BindView(R.id.edttxt_chat_box)
    EditText edttxt_chat_box;
    @BindView(R.id.img_send)
    ImageView img_send;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_channel);
        ButterKnife.bind(this);
        context = this;

        nReceiver = new NotificationReceiver();
        IntentFilter filter = new IntentFilter();
        filter.addAction(Appconst.BROADCAST_EXTRA);
        registerReceiver(nReceiver, filter);


        setClicks();
        getPassedData();
        getChannelHistory();
        checkRtmStatus();


    }

    private void setClicks() {

        img_send.setOnClickListener(this);
    }

    private void getPassedData() {

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {

            channel = bundle.getString("channel");
            channel_name = bundle.getString("channel_name");
            txt_channel_name.setText(getString(R.string.channel_name, channel_name));
            setToolBar(channel_name,"");
            getChannelHistory();
        }

    }

    private void getChannelHistory() {

        GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
        Call<ChannelHistoryModel> channelHistoryModelCall = service.channelHistory(SharedPref.getAccessToken(),
                channel);
        channelHistoryModelCall.enqueue(new Callback<ChannelHistoryModel>() {
            @Override
            public void onResponse(Call<ChannelHistoryModel> call, Response<ChannelHistoryModel> response) {

                if (response.isSuccessful()) {

                    channelHistoryModel = response.body();
                    setAdapter();

                }
            }

            @Override
            public void onFailure(Call<ChannelHistoryModel> call, Throwable t) {

            }
        });
    }


    private void setAdapter() {

        channelAdapter = new ChannelAdapter(rv_channels_main, channelHistoryModel.getMessageFromChannel(), context);
        rv_channels_main.setHasFixedSize(true);
        rv_channels_main.setAdapter(channelAdapter);
        progressBar.setVisibility(View.GONE);
        rv_channels_main.setVisibility(View.VISIBLE);

    }

    private void checkRtmStatus() {

        GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
        Call<RtmBaseModel> checkRtmSuccess = service.checkRtmSuccess(SharedPref.getAccessToken());
        checkRtmSuccess.enqueue(new Callback<RtmBaseModel>() {
            @Override
            public void onResponse(Call<RtmBaseModel> call, Response<RtmBaseModel> response) {

                if (response.isSuccessful()) {

                    rtmBaseModel = response.body();
                    Log.e("rtmBaseModel", new Gson().toJson(rtmBaseModel));
                    connectRtmServer();

                }
            }

            @Override
            public void onFailure(Call<RtmBaseModel> call, Throwable t) {

            }
        });
    }

    private void connectRtmServer() {


        webSocket = new OkHttpClient().newWebSocket(new Request.Builder().url(rtmBaseModel.getUrl()).build(), new WebSocketListener() {
            @Override
            public void onOpen(WebSocket webSocket, okhttp3.Response response) {
                super.onOpen(webSocket, response);
                Log.e("onOpen_response", new Gson().toJson(response));
            }

            @Override
            public void onMessage(WebSocket webSocket, String text) {
                super.onMessage(webSocket, text);
                Log.e("onMessage", text);
                MessageRecieveModel messageModel = new Gson().fromJson(text, MessageRecieveModel.class);
                Log.e("messageModel", new Gson().toJson(messageModel));
                if (messageModel.getType().equals(Appconst.MESSAGE_TYPE_MESSAGE)) {

                    getChannelHistory();
                    if (!SharedPref.getUserId().equals(messageModel.getUser())) {

                        notification_text = messageModel.getText();
                        Intent i = new Intent(Appconst.BROADCAST_EXTRA);
                        i.putExtra("command", "list");
                        sendBroadcast(i);
                    }


                }


            }

            @Override
            public void onClosed(WebSocket webSocket, int code, String reason) {
                super.onClosed(webSocket, code, reason);
                Log.e("onClosed", reason);
            }
        });


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.img_send:
                count++;
                sendMessage();
                break;
        }
    }

    private void sendMessage() {


        Log.e("sendMessage()", "called");
        MessageModel messageModel = new MessageModel();
        messageModel.setId(count);
        messageModel.setChannel(channel);
        messageModel.setType(Appconst.MESSAGE_TYPE_MESSAGE);
        messageModel.setText(Utils.getString(edttxt_chat_box));
        edttxt_chat_box.setText("");
        webSocket.send(new Gson().toJson(messageModel));
        checkRtmStatus();


    }


    class NotificationReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {

            pushNotification();
        }
    }


    private void pushNotification() {

        //Get an instance of NotificationManager//

        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(R.drawable.ic_notification_icon)
                        .setContentTitle("Slack Notification")
                        .setContentText(notification_text);

        PendingIntent contentIntent = PendingIntent.getActivity(this, 0,
                new Intent(this, HomeActivity.class), PendingIntent.FLAG_UPDATE_CURRENT);
        mBuilder.setContentIntent(contentIntent);
        NotificationManager mNotificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.notify(1, mBuilder.build());
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        webSocket.cancel();
        unregisterReceiver(nReceiver);

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}




