package com.example.clintpaul.slacktestapp;

import android.app.Application;

public class MyApplication extends Application {

    private static MyApplication sInstance;

    @Override
    public void onCreate() {
        super.onCreate();
        sInstance = this;
    }

    public MyApplication() {
        sInstance = this;
    }

    public static synchronized MyApplication getInstance() {
        return sInstance;
    }
}
