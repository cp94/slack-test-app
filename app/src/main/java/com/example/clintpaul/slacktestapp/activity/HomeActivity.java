package com.example.clintpaul.slacktestapp.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.clintpaul.slacktestapp.R;
import com.example.clintpaul.slacktestapp.adapter.ChannelListAdapter;
import com.example.clintpaul.slacktestapp.base.BaseActivity;
import com.example.clintpaul.slacktestapp.model.AccessTokenModel;
import com.example.clintpaul.slacktestapp.model.ChannelInfoModel;
import com.example.clintpaul.slacktestapp.network.retrofit.GetDataService;
import com.example.clintpaul.slacktestapp.network.retrofit.RetrofitClientInstance;
import com.example.clintpaul.slacktestapp.utils.Appconst;
import com.example.clintpaul.slacktestapp.utils.SharedPref;
import com.example.clintpaul.slacktestapp.utils.Utils;
import com.google.gson.Gson;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeActivity extends BaseActivity implements View.OnClickListener {

    @BindView(R.id.txt_add_to_slack)
    TextView txt_add_to_slack;
    @BindView(R.id.lin_user_details)
    LinearLayout lin_user_details;
    @BindView(R.id.txt_user_name)
    TextView txt_user_name;
    @BindView(R.id.rv_channels)
    RecyclerView rv_channels;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;

    private Context context;
    private String code;
    private AccessTokenModel accessTokenModel;
    private ChannelInfoModel channelInfoModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        context = this;
        setClicks();
        setToolBarWithoutBack(getString(R.string.home));

        if (Utils.isNotEmpty(SharedPref.getAccessToken())) {

            setViews();

        } /*else {

            getAccessToken();
        }*/

    }

    private void setClicks() {

        txt_add_to_slack.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.txt_add_to_slack:

                Intent intent = new Intent(context, WebViewActivity.class);
                startActivityForResult(intent, Appconst.REQUEST_CODE_WEBVIEW);
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {

        try {
            super.onActivityResult(requestCode, resultCode, data);
            if (requestCode == Appconst.REQUEST_CODE_WEBVIEW  && resultCode  == RESULT_OK) {
                assert data != null;
                code = data.getStringExtra("code");
                Log.e("code_home", code);
                if (SharedPref.getAccessToken().isEmpty()) {

                    getAccessToken();
                } else {

                    setViews();
                }



            }
        } catch (Exception ex) {
            showToast("Some error occurred");
        }

    }

    private void getAccessToken() {

        Log.e("getAccessToken_called","called");
        GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
        final Call<AccessTokenModel> accessTokenModelCall = service.getAccessToken(
                Appconst.CLIENT_ID,
                Appconst.CLIENT_SECRET,
                code,
                Appconst.REDIRECT_URI,
                true
        );
        accessTokenModelCall.enqueue(new Callback<AccessTokenModel>() {
            @Override
            public void onResponse(@NonNull Call<AccessTokenModel> call, @NonNull Response<AccessTokenModel> response) {

                    if (response.isSuccessful()) {

                        Log.e("response_code", String.valueOf(response.code()));
                        Log.e("response_is", new Gson().toJson(response.body()));
                        accessTokenModel = response.body();
                        assert accessTokenModel != null;
                        Log.e("access_token_is",accessTokenModel.getAccess_token());
                        SharedPref.setAccessToken(accessTokenModel.getAccess_token());
                        SharedPref.setUserId(accessTokenModel.getUser_id());
                        setViews();
                    }



                }


            @Override
            public void onFailure(@NonNull Call<AccessTokenModel> call, @NonNull Throwable t) {

            }
        });




    }

    private void setViews() {

        lin_user_details.setVisibility(View.VISIBLE);
        txt_add_to_slack.setVisibility(View.GONE);
        getChannelInfo();
//        txt_user_name.setText(SharedPref.getUserId());

    }

    private void getChannelInfo() {

        GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
        Call<ChannelInfoModel> channelInfoModelCall = service.getChannelInfo(SharedPref.getAccessToken());
        channelInfoModelCall.enqueue(new Callback<ChannelInfoModel>() {
            @Override
            public void onResponse(@NonNull Call<ChannelInfoModel> call, @NonNull Response<ChannelInfoModel> response) {

                if (response.isSuccessful()) {

                    channelInfoModel = response.body();
                    Log.e("channelInfoModel", new Gson().toJson(channelInfoModel));
                    setChannelsView();
                }
            }

            @Override
            public void onFailure(@NonNull Call<ChannelInfoModel> call, @NonNull Throwable t) {

            }
        });

    }

    private void setChannelsView() {

        ChannelListAdapter channelListAdapter = new ChannelListAdapter(rv_channels, channelInfoModel.channelDetails, context);
        rv_channels.setHasFixedSize(true);
        rv_channels.setAdapter(channelListAdapter);
        progressBar.setVisibility(View.GONE);
        rv_channels.setVisibility(View.VISIBLE);

    }
}
