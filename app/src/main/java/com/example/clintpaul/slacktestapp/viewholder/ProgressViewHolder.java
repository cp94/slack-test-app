package com.example.clintpaul.slacktestapp.viewholder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;

import com.example.clintpaul.slacktestapp.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ProgressViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.progressBar) public ProgressBar progressBar;

    public ProgressViewHolder(View v) {
        super(v);
        ButterKnife.bind(this, v);
    }
}
