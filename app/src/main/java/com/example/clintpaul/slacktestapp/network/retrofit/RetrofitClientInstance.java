package com.example.clintpaul.slacktestapp.network.retrofit;

import com.example.clintpaul.slacktestapp.BuildConfig;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitClientInstance {

    private static Retrofit retrofit;
//    private static final String BASE_URL = BuildConfig.SERVER_URL;
    private static final String BASE_URL = "https://slack.com/api/";
    private static OkHttpClient.Builder defaultHttpClient = new OkHttpClient.Builder();


    public static Retrofit getRetrofitInstance() {
        if (retrofit == null) {
            retrofit = new retrofit2.Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(getClient())
//                    .client(getHeader(Base64.encodeToString(Appconst.AUTHORIZATION.getBytes(),Base64.NO_WRAP)))
                    .build();
        }
        return retrofit;
    }


    public static OkHttpClient getClient() {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
        return client;
    }




}
