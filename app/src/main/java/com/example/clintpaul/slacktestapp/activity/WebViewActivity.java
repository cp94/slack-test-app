package com.example.clintpaul.slacktestapp.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.example.clintpaul.slacktestapp.R;
import com.example.clintpaul.slacktestapp.base.BaseActivity;
import com.example.clintpaul.slacktestapp.utils.Appconst;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Set;

import butterknife.BindView;
import butterknife.ButterKnife;

public class WebViewActivity extends BaseActivity {

    @BindView(R.id.webview)
    WebView webview;

    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_view);
        ButterKnife.bind(this);
        Context context = this;

        Uri builtUri = Uri.parse(Appconst.AUTH_URL)
                .buildUpon()
                .appendQueryParameter(Appconst.CLIENT_ID_KEY, Appconst.CLIENT_ID)
                .appendQueryParameter(Appconst.SCOPE_KEY, Appconst.SCOPE)
                .appendQueryParameter(Appconst.REDIRECT_URI_KEY, Appconst.REDIRECT_URI)
                .build();


        WebSettings webSettings = webview.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webview.setScrollBarStyle(View.SCROLLBARS_OUTSIDE_OVERLAY);
//        webview.getSettings().setBuiltInZoomControls(true);
        webview.getSettings().setUseWideViewPort(true);
        webview.getSettings().setLoadWithOverviewMode(true);
        progressDialog = new ProgressDialog(context);
        progressDialog.setMessage("Loading...");
        progressDialog.show();



        webview.setWebChromeClient(new WebChromeClient());


        webview.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);

                Log.d("WebView", "your current url when webpage loading.." + url);


            }

            @Override
            public void onPageFinished(WebView view, String url) {
                Log.d("WebView", "your current url when webpage loading.. finish" + url);
                super.onPageFinished(view, url);

                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }

                Log.e("onPageFinished_url", url);
                Uri uri = Uri.parse(url);
                String server = uri.getAuthority();
                String path = uri.getPath();
                String protocol = uri.getScheme();
                Set<String> args = uri.getQueryParameterNames();
                if (uri.getQueryParameter("code") != null) {
                    Log.e("parameters are", String.valueOf(uri.getQueryParameter("code")));
                    Intent intent = new Intent();
                    intent.putExtra("code", String.valueOf(uri.getQueryParameter("code")));
                    setResult(RESULT_OK, intent);
                    finish();
                }



            }

            @Override
            public void onLoadResource(WebView view, String url) {
                // TODO Auto-generated method stub
                super.onLoadResource(view, url);
            }

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                System.out.println("when you click on any interlink on webview that time you got url :-" + url);
                return super.shouldOverrideUrlLoading(view, url);
            }


        });

        try {
            URL url = new URL(builtUri.toString());
            webview.loadUrl(String.valueOf(url));
            Log.e("auth_url_is", String.valueOf(url));
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }


}
