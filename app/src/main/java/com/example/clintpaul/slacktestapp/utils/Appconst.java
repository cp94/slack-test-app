package com.example.clintpaul.slacktestapp.utils;

public class Appconst {


    public static final String AUTH_URL = "https://slack.com/oauth/authorize";
    public static final String CLIENT_ID_KEY = "client_id";
    public static final String SCOPE_KEY = "scope";
    public static final String REDIRECT_URI_KEY = "redirect_uri";
    public static final String SINGLE_CHANNEL_KEY = "single_channel";
    public static final String CLIENT_SECRET_KEY = "client_secret";
    public static final String CODE_KEY = "code";
    public static final String STATE_KEY = "state";
    public static final String TEAM_KEY = "team";
//    public static final String CLIENT_ID = "465807052516.510123431538";
public static final String CLIENT_ID = "362211397057.532603656838";
    public static final String SCOPE = "client";
    public static final String REDIRECT_URI = "http://localhost";
    public static final String CLIENT_SECRET = "030907d51dc13c103f050cff70e2a6eb";
//    public static final String CLIENT_SECRET = "b59687a756b0c60ba22e03949021eb04";
    public static final int REQUEST_CODE_WEBVIEW = 1;
    public static final String MESSAGE_TYPE_MESSAGE = "message";
    public static final String BROADCAST_EXTRA = "com.example.clintpaul.slacktestapp.NOTIFICATION_LISTENER_EXAMPLE";

//    362211397057.532603656838


}
