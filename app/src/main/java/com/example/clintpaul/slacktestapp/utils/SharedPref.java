package com.example.clintpaul.slacktestapp.utils;

import android.content.Context;
import android.content.SharedPreferences;

import com.example.clintpaul.slacktestapp.MyApplication;

public class SharedPref {

    private static SharedPreferences prefs;
    private static String S_PREFS_NAME = "app_slack_test_app_prefs";
    private static String S_PREFS_ACCESS_TOKEN = "pref_access_token";
    private static String S_PREFS_USER_NAME = "pref_user_id";


    public static SharedPreferences getSharedPref() {
        if (prefs == null) {
            prefs = MyApplication.getInstance().getSharedPreferences(S_PREFS_NAME,
                    Context.MODE_PRIVATE);
        }
        return prefs;
    }

    public static void setAccessToken(String authKey) {

        prefs = getSharedPref();
        prefs.edit().putString(S_PREFS_ACCESS_TOKEN, authKey).apply();
    }

    public static String getAccessToken() {

        prefs = getSharedPref();
        return prefs.getString(S_PREFS_ACCESS_TOKEN, "");
    }


    public static void setUserId(String userId) {

        prefs = getSharedPref();
        prefs.edit().putString(S_PREFS_USER_NAME, userId).apply();
    }

    public static String getUserId() {

        prefs = getSharedPref();
        return prefs.getString(S_PREFS_USER_NAME, "");
    }


}
